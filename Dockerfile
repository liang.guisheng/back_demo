FROM python:3.7-slim-buster

RUN apt update -y && apt install -y gcc

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

ADD deploy/{{PROJECT_PACKAGE_NAME}}-SNAPSHOT.tar.gz /usr/src/

RUN mv /usr/src/{{PROJECT_PACKAGE_NAME}}*/* /usr/src/app && ls -la && pip install -r requirements.txt && pip install gunicorn

EXPOSE 8000

CMD ["ddtrace-run", "gunicorn", "-b", "0.0.0.0:8000", "-w", "4", "--access-logfile", "-", "--error-logfile", "-", "uwsgi:App"]