#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 8/28/19 9:42 AM
@Author: liang
@File: setup.py
"""
import os
import subprocess

from setuptools import setup, find_namespace_packages


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(file_name):
    try:
        with open(os.path.join(os.path.dirname(__file__), file_name)) as content:
            return content.read()
    except FileNotFoundError:
        return None


def parse_requirements(content):
    if not content:
        raise Exception('Unable to find requirements.txt file!')
    return [r for r in content.split('\n') if r and not r.strip().startswith('-')]


def version():
    git_version = None
    try:
        git_tag = subprocess.check_output(['git', 'describe', '--tags'])
        return git_tag.strip()[1:].decode('utf-8').split('-')[0] if git_tag else 'SNAPSHOT'
    except Exception:
        return 'SNAPSHOT'


project_packages = find_namespace_packages()

setup(name='{{PROJECT_NAME}}',
      version=version(),
      description='{{PROJECT_DESCRIPTION}}',
      author='Truckpad Dev',
      author_email='engenharia@truckpad.com.br',
      long_description=read('README.md'),
      long_description_content_type='text/markdown',
      url='https://github.com/truckpad/backend-api-demo',
      packages=project_packages,
      install_requires=parse_requirements(read('requirements.txt')),
      include_package_data=True,
      classifiers=[
          'Development Status :: 2 - Pre-Alpha', 'Operating System :: POSIX', 'Programming Language :: Python :: 3',
          'Topic :: Internet :: WWW/HTTP'
      ])
