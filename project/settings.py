#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" # pragma: no cover
@Creation: 19/05/2020  19:41
@Author: liang
@File: settings.py
"""
# plain python row value configurations
MONGO_DEMO_COLLECTION = 'demos'

# environment sensible configurations
API_ENV = '${API_ENV:homolog}'  # pragma: no cover
MONGO_DB_URL = '${MONGO_DB_URL:mongodb://localhost:27017<str>}'

# environment plain text configurations
_config_files_ = ['config.ini']
MONGO_DB_NAME = None
