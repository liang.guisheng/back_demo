#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 8/28/19 9:42 AM
@Author: liang
@File: app.py
"""
import bottle
from bottle import Bottle

from project.feature_mock import patch_setting_module

patch_setting_module('project.settings', debug=True)  # init global settings module before import other project module
from project.api.demo_api_v0 import demo_app_v0  # noqa

bottle.debug(True)
App = Bottle()

App.mount('/v0', demo_app_v0)

if __name__ == '__main__':
    App.run(host='0.0.0.0', port=8000, debug=True, reloader=True)  # pragma: no cover
