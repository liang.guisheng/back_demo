#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 8/28/19 9:42 AM
@Author: liang
@File: __init__.py.py
"""
import gettext
import os

el = gettext.translation('base', localedir=str(os.path.dirname(__file__)),
                         languages=[os.environ.get('LANGUAGE', 'pt_BR')])
el.install()
