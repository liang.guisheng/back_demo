#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 8/28/19 9:44 AM
@Author: liang
@File: schema.py
"""
from datetime import datetime

from marshmallow import Schema, EXCLUDE
from marshmallow.fields import UUID, String, Nested, DateTime, Email
from pymongo import MongoClient

from project import settings
from project.feature_mock import SerializerMixin, MarshmallowMongoMixin


class ActionLog(Schema):
    datetimeformat = '%Y-%m-%dT%H:%M:%S.%fZ'  # format for datetime field

    id = UUID(required=False)
    name = String(required=False)
    email = Email(required=False)
    at = DateTime(missing=datetime.now)


class DemoSchema(Schema, MarshmallowMongoMixin, SerializerMixin):
    id = UUID(required=False)
    name = String(required=True)
    create_log = Nested(ActionLog, required=False)
    update_log = Nested(ActionLog, required=False)

    class Meta:
        unknown = EXCLUDE
        ordered = True
        # monte mongo connection here
        db = MongoClient(settings.MONGO_DB_URL).get_database(settings.MONGO_DB_NAME).get_collection(
            settings.MONGO_DEMO_COLLECTION)
