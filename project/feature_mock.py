#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 19/05/2020  19:52
@Author: liang
@File: feature_mock.py
"""
import json
import logging
import os
import re
from configparser import ConfigParser
from importlib import import_module
from pathlib import Path
from typing import List, Union, Dict, Optional, Any, Set, Sequence, Callable, Type
from uuid import uuid4

from bottle import HTTPError, HTTPResponse, request, MultiDict, JSONPlugin, abort, Bottle, Route, response
from marshmallow import post_load, pre_dump
from pymongo.collection import Collection

from project.locales import el

_ = el.gettext
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


############################################################
# Micro-service related plugin which includes:
#   1. Authenticator with AuthUser object
#   2. Unify the http response result
#   3. Payload required validation
############################################################

class AuthUser:
    def __init__(self, user_info: Dict = None):
        self._info = user_info or {}

    @property
    def id(self) -> Optional[str]:
        return self._info.get('id')

    @property
    def name(self) -> Optional[str]:
        return self._info.get('name', '')

    @property
    def email(self) -> Optional[str]:
        return self._info.get('email', '')

    @property
    def type(self) -> Optional[str]:
        return self._info.get('type')

    @property
    def roles(self) -> Set:
        return {o for o in self._info.get('roles') if o}

    @property
    def is_authenticated(self):
        """
        Mock method to check if  current user object is authenticated, which should contains all
        the following fields: 'id', 'type', 'roles'
        """
        return all([self._info.get(k) for k in {'id', 'type', 'roles'}])


class AuthorizationParser:

    def __init__(self, user_class: Type = AuthUser):
        self._user_class = user_class

    def extract_credential(self) -> Optional[Dict]:
        return {
            'id': request.headers.get('x-user-id'),
            'roles': {o for o in request.headers.get('x-user-role', '').split(',') if o},
            'type': request.headers.get('x-user-type')
        }

    def is_authenticated(self, user_types: Sequence[str] = None, user_roles: Sequence[str] = None) -> Optional[bool]:
        user_obj: AuthUser = self._user_class(self.extract_credential())

        if user_types and user_obj.type not in user_types:
            abort(403, _('unauthenticated_user'))

        if user_roles and not user_obj.roles.intersection(set(user_roles)):
            abort(403, _('unauthenticated_user'))

        request.user = user_obj
        return user_obj.is_authenticated


# TODO: implement a mocked plain json text authorization parser
class JsonAuthorizationParser(AuthorizationParser):
    pass


# TODO: implement the jwt access_token authorization parser
class JwtAuthorizationParser(AuthorizationParser):
    pass


class BottleMSPlugin:
    """
    Features for micro-service:
        1. authentication
        2. payload_validate
        3. unify error/success response result
    """
    name = 'authorization_plugin'
    api = 2

    LOGIN_REQUIRED_KEY = '__login_required__'
    PAYLOAD_REQUIRED_KEY = '__payload_required__'
    USER_TYPE_REQUIRED_KEY = '__user_type_required__'
    USER_ROLE_REQUIRED_KEY = '__user_role_required__'

    # pylint: disable=too-many-arguments
    def __init__(self,
                 authenticator: AuthorizationParser,
                 json_dumper: Callable = None,
                 message_key: str = 'message',
                 default_message: str = '',
                 status_key: str = 'status',
                 default_error_handler: Callable = None,
                 restful_success_handler: Callable = None,
                 authenticate_handler: Callable = None,
                 payload_required_handler: Callable = None
                 ):
        self.authenticator = authenticator
        self.json_dumper = json_dumper
        self.message_key = message_key
        self.default_message = default_message
        self.status_key = status_key
        self.authenticate_handler = authenticate_handler
        self.payload_required_handler = payload_required_handler
        self.default_error_handler = default_error_handler
        self.restful_success_handler = restful_success_handler

    def setup(self, app: Bottle):  # noqa
        """ configure the plugin for current application object"""

        if not self.json_dumper:
            for plugin in app.plugins:
                if isinstance(plugin, JSONPlugin):
                    self.json_dumper = plugin.json_dumps
                    break

        def restful_error_handler(error_response):
            """ Specify default error handler for bottle application object """
            error_response.content_type = 'application/json'

            # Catch non-http error and return the message with the standard message structure format
            if isinstance(error_response, HTTPError) and error_response.exception:
                return self.json_dumper({self.status_key: 500, self.message_key: str(error_response.exception)})

            try:
                body = json.loads(error_response.body)
                if self.status_key not in body:
                    return self.json_dumper(
                        {self.status_key: error_response.status_code, self.message_key: body})
                return error_response.body
            except:  # noqa
                return self.json_dumper(
                    {self.status_key: error_response.status_code, self.message_key: error_response.body})

        def restful_response_handler(response_result):
            """ This method used to unify the success response's result """

            # If the view function return a `dict` object, then patch the standard fields into the dict and return
            if isinstance(response_result, dict):
                response_result.update({self.message_key: self.default_message, self.status_key: response.status_code})
                return response_result

            # If the view function return a 'HTTPResponse' object with 'dict' object, then patch
            # the standard fields into the response object's body
            if isinstance(response_result, HTTPResponse) and isinstance(response_result.body, dict):
                response_result.body.update(
                    {self.message_key: self.default_message, self.status_key: response.status_code})
                return response_result

            # Then if the result is a string or the HTTPResponse object's body is a string, patch the standard
            # fields into the final result
            return {self.message_key: response_result or self.default_message, self.status_key: response.status_code}

        def authenticate_handler(callback: Callable):
            """ This method used to handle authentication related  topics """
            is_login_required = getattr(callback, self.LOGIN_REQUIRED_KEY, False)
            required_user_types_value = getattr(callback, self.USER_TYPE_REQUIRED_KEY, None)
            required_user_roles_value = getattr(callback, self.USER_ROLE_REQUIRED_KEY, None)

            if any([is_login_required, required_user_roles_value, required_user_types_value]):
                if not self.authenticator.is_authenticated(user_types=required_user_types_value,
                                                           user_roles=required_user_roles_value):
                    abort(403, _('unauthenticated_user'))

        def payload_required_handler(callback: Callable):
            if getattr(callback, self.PAYLOAD_REQUIRED_KEY, False) and not request.json:
                abort(400, _('bad_request_data'))

        def before_request_hook():
            """ This method will be called before every http request call """
            logger.info('Before request\n')

        def after_request_hook():
            """ This method will be called after every http request call """
            logger.info('Finished request\n')

        # configure current plugin instance object
        self.authenticate_handler = self.authenticate_handler or authenticate_handler
        self.payload_required_handler = self.payload_required_handler or payload_required_handler
        self.restful_success_handler = self.restful_success_handler or restful_response_handler

        # configure the current bottle application object
        app.default_error_handler = self.default_error_handler or restful_error_handler
        app.add_hook('before_request', before_request_hook)
        app.add_hook('after_request', after_request_hook)

    def apply(self, callback: Callable, route: Route):  # noqa
        """ All special treatments for view function are handled here """

        def view_decorator(*args, **kwargs):
            self.authenticate_handler(callback)
            self.payload_required_handler(callback)
            return self.restful_success_handler(callback(*args, **kwargs))

        return view_decorator

    @staticmethod
    def payload_required(view_func: Callable) -> Callable:
        setattr(view_func, BottleMSPlugin.PAYLOAD_REQUIRED_KEY, True)
        return view_func

    @staticmethod
    def login_required(view_func: Callable) -> Callable:
        """ Create a login required marker for the view_func """
        setattr(view_func, BottleMSPlugin.LOGIN_REQUIRED_KEY, True)
        return view_func

    @staticmethod
    def required_user_types(user_types: Sequence[str] = None) -> Callable:
        """ Create a user type marker for the view function """

        def view_decorator(view_func: Callable) -> Callable:
            setattr(view_func, BottleMSPlugin.LOGIN_REQUIRED_KEY, True)
            setattr(view_func, BottleMSPlugin.USER_TYPE_REQUIRED_KEY, set(user_types))
            return view_func

        return view_decorator

    @staticmethod
    def required_user_roles(user_roles: Sequence[str] = None) -> Callable:
        """ Create a user role marker for the view function """

        def view_decorator(view_func: Callable) -> Callable:
            setattr(view_func, BottleMSPlugin.LOGIN_REQUIRED_KEY, True)
            setattr(view_func, BottleMSPlugin.USER_ROLE_REQUIRED_KEY, set(user_roles))
            return view_func

        return view_decorator


############################################################
# Marshmallow based serialization and mongo function mixin
############################################################
class SerializerMixin:

    @classmethod
    def _get_serializer_class(cls, http_query: MultiDict = None):

        http_query = http_query or MultiDict()

        serializer_meta_option = {}
        if http_query.getlist('include'):
            serializer_meta_option['fields'] = http_query.getlist('include')
        if http_query.getlist('exclude'):
            serializer_meta_option['exclude'] = http_query.getlist('exclude')
        serializer_attrs = {'Meta': type('serializer_meta', (object,), serializer_meta_option)}
        return type(f'{cls.__name__}Serializer', (cls,), serializer_attrs)

    @classmethod
    def serialize(cls, http_query: MultiDict, data: Union[Dict, List]):
        serializer = cls._get_serializer_class(http_query)()
        return serializer.dump(data, many=not isinstance(data, Dict))


class MarshmallowMongoMixin:

    @classmethod
    def db(cls) -> Collection:
        return cls.Meta.db

    @post_load
    def post_load_handler(self, data, **kwargs):  # noqa
        if self.context.get('action') == 'create':
            data['_id'] = data.pop('id', uuid4())  # Before save we need to make sure the `_id` field

        if self.context.get('action') == 'update':
            data.pop('id', None)  # For update, we don't need to load the `id` field

        return data

    @pre_dump
    def pre_dump_handler(self, data, **kwargs):  # noqa
        if '_id' in data:
            data['id'] = data.pop('_id')
        return data


############################################################
# Self created project global setting helper
############################################################

def patch_setting_module(setting_module_name: str, api_env_key: str = None, config_file_key: str = None,
                         valid_envs: List[str] = None, debug: bool = False):
    ModuleSettings(setting_module_name, api_env_key, config_file_key, valid_envs, debug)


def boolean(value: str):
    return value.lower() not in ['', 'f', 'n', '0', 'false']


class ModuleSettings:
    CONFIG_FILE_KEY = '_config_files_'
    ENV_VALUE_PATTERN = r'(\$\{([^:]+)?:?(.+?)?(?:<(int|bool|str|float)>)?\})'  # ${ENV_name:DEFAULT_VALUE<type>}
    NORMAL_VALUE_PATTERN = r'(.+)?(?:<(int|bool|str|float)>)'
    API_ENV_KEY = 'API_ENV'
    VALID_ENVS = ['default', 'test', 'development', 'homolog', 'staging', 'production']
    CONVERTER_MAP = {
        'int': int,
        'str': str,
        'bool': boolean,
        'float': float
    }

    # pylint: disable=too-many-arguments
    def __init__(self,
                 setting_module_name: str,
                 api_env_key: str = None,
                 config_file_key: str = None,
                 valid_envs: List[str] = None,
                 debug: bool = False
                 ):
        self.debug = debug
        try:
            self._setting_module_name = setting_module_name
            self._module = import_module(setting_module_name)
        except Exception as e:
            raise e
        else:

            if api_env_key:
                self.API_ENV_KEY = api_env_key

            if valid_envs:
                self.VALID_ENVS = valid_envs

            if config_file_key:
                self.CONFIG_FILE_KEY = config_file_key

            self._module_path = Path(self._module.__file__).parent
            self._module_options = self._extract_module_options()
            self._module_config_files = getattr(self._module, self.CONFIG_FILE_KEY, [])

            # we need to detect the environment before parse file configurations
            self.current_env = self._parser_option_value(self._module_options.get(self.API_ENV_KEY))
            self._file_options = self._extract_config_file_options()

            self._merge_files_options()

            self._patch_setting_module_option_values()

    def _merge_files_options(self):
        """
        Merge the configuration options into the module options
        """
        non_valid_options = set(self._file_options.keys()) - set(self._module_options.keys())
        if non_valid_options:
            raise Warning(
                f'Configuration fields: {non_valid_options} should be declared in {self._setting_module_name}!')
        self._module_options.update(self._file_options)

    def _patch_setting_module_option_values(self):
        for k, v in self._module_options.items():
            if v is not None:
                v = self._parser_option_value(v)
                self._module_options[k] = v
                if self.debug:
                    print(f'{k}  => {v}')
                setattr(self._module, k, v)

    def _extract_module_options(self) -> Dict:
        """
       Extract all configuration options from the given settings module
        """
        return {k: getattr(self._module, k) for k in dir(self._module) if k.isupper()}

    def _extract_config_file_options(self) -> Dict:
        """
        Extract all configuration options from configuration files from the given settings module
        """
        parser = ConfigParser()
        global_file_options = {}
        for filename in self._module_config_files:
            config_path = self._module_path / filename
            if not config_path.exists():
                raise Warning(f'Configuration file: {config_path} not found!')
            parser.read(config_path)
            sections = parser.sections()
            non_valid_sections = set(sections) - set(self.VALID_ENVS)
            if non_valid_sections:
                raise Warning(f'Invalid config sections: {sections} in file {config_path}')
            global_file_options.update(parser.items('default') if parser.has_section('default') else {})
            global_file_options.update(
                parser.items(self.current_env) if parser.has_section(self.current_env) else {})
        return {key.upper(): value for key, value in global_file_options.items()}

    def _parser_option_value(self, config_value: str) -> Optional[Any]:
        """
        Parse the config option's value which consider the environment variable and datatype
        """
        if not isinstance(config_value, str):
            return config_value
        match = re.match(self.ENV_VALUE_PATTERN, config_value)
        if match:
            env_name, default_value, value_type = match.groups()[1:]
            final_value = os.environ.get(env_name, default_value)
            if value_type:
                return self.CONVERTER_MAP[value_type](final_value)
            return final_value

        match = re.match(self.NORMAL_VALUE_PATTERN, config_value)
        if match:
            value, value_type = match.groups()
            if value_type:
                return self.CONVERTER_MAP.get(value_type, str)(value)

        return config_value
