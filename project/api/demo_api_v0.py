#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 8/28/19 9:44 AM
@Author: liang
@File: demo_api_v0.py
"""
from uuid import UUID

from bottle import Bottle, request, abort, response, HTTPResponse

from project.feature_mock import (BottleMSPlugin, AuthorizationParser)
from project.locales import el
from project.schema.demo_schema_v0 import DemoSchema

_ = el.gettext

demo_app_v0 = Bottle()
ms_plugin = BottleMSPlugin(AuthorizationParser())
demo_app_v0.install(ms_plugin)


@demo_app_v0.get('/demos')
@ms_plugin.login_required
def get_demo_list():
    db_query = {}
    if 'operator' in request.user.roles:
        db_query.update({'create_log.id': UUID(request.user.id)})
    db_objects = DemoSchema.db().find(db_query)
    objects = DemoSchema.serialize(request.query, db_objects)
    return {'objects': objects}


@demo_app_v0.get('/demo/<demo_id>')
@ms_plugin.login_required
def get_demo_by_id(demo_id: str):
    target_obj = DemoSchema.db().find_one({'_id': UUID(demo_id)})
    if not target_obj:
        abort(404, _('demo not found'))
    if request.user.id != str(target_obj.get('create_log').get('id')) and 'admin' not in request.user.roles:
        abort(403, _('unauthenticated_user'))
    return {'object': DemoSchema.serialize(request.query, target_obj)}


@demo_app_v0.post('/demos')
@ms_plugin.payload_required
@ms_plugin.required_user_roles(user_roles=['admin', 'operator'])
def create_demo_object():
    payload = request.json
    payload.update({'create_log': {
        'id': request.user.id,
        'name': request.user.name,
        'email': request.user.email or 'test@mail.com'  # for test coverage
    }
    })

    validate_error = DemoSchema().validate(payload)
    if validate_error:
        abort(422, validate_error)

    valid_data = DemoSchema(context={'action': 'create'}).load(payload)
    DemoSchema.db().insert_one(valid_data)
    response.status = 201
    if request.query.get('return_new', False):
        return {'object': DemoSchema.serialize(request.query, valid_data)}
    return {'object': {'id': str(valid_data.get('_id'))}}


@demo_app_v0.put('/demo/<demo_id>')
@ms_plugin.required_user_roles(user_roles=['admin', 'operator'])
@ms_plugin.required_user_types(user_types=['admin', 'operator'])
@ms_plugin.payload_required
def update_demo(demo_id: str):
    """
    Update the resource demo object by its id 
    """
    payload = request.json
    valid_error = DemoSchema().validate(payload)
    if valid_error:
        abort(422, valid_error)
    target_obj = DemoSchema.db().find_one({'_id': UUID(demo_id)})
    if not target_obj:
        abort(404, _('object_not_found'))
    if 'admin' not in request.user.roles and str(target_obj.get('create_log').get('id')) != request.user.id:
        abort(403, _('unauthenticated_user'))

    payload.update({'update_log': {
        'id': request.user.id,
        'name': request.user.name
    }})

    valid_data = DemoSchema(context={'action': 'update'}).load(payload)

    DemoSchema.db().update_one({'_id': UUID(demo_id)}, {'$set': valid_data})

    target_obj.update(valid_data)

    response.status = 202
    if request.query.get('return_new', False):
        return {'object': DemoSchema.serialize(request.query, target_obj)}
    return HTTPResponse({'object': {'id': demo_id}}, 202)


@demo_app_v0.delete('/demo/<demo_id>')
@ms_plugin.required_user_roles(user_roles=['admin', 'operator'])
def delete_demo_by_id(demo_id: str):
    target_obj = DemoSchema.db().find_one({'_id': UUID(demo_id)})

    if not target_obj:
        abort(404, {'message': _('object_not_found'), 'status': 404})  # for test coverage

    if str(target_obj.get('create_log').get('id')) != request.user.id and 'admin' not in request.user.roles:
        abort(403, _('unauthenticated_user'))

    DemoSchema.db().delete_one({'_id': UUID(demo_id)})
    response.status = 203
    return 'delete success'


@demo_app_v0.get('/demo/error_case')
def non_http_error_test_coverage_case():
    raise Exception('for test coverage')
