## 1. msv project structure suggestions

- internal_servcie: refer to our own micro-service invocation
- external_service: refer to 3rd party external service invocation
- helper: wrapper any complicated/redundancy logic codes if needed<replace *[business/repository/model]* directory>
- main.py/app.py: the entry point of our project
- settings.py: the project scope configurations 

<pre>

    project/
    │  
    ├── app.py
    │  
    ├── config.ini
    │  
    ├── feature_mock.py
    │  
    ├── settings.py
    │  
    ├── api
    │   │
    │   └── demo_api_v0.py
    │  
    ├── schema
    │   │
    │   └── demo_schema_v0.py
    │  
    └── service/business/utils
        │
        └── xxxx.py
                   
</pre>            

        
## 2. HTTP response standard recommendations

### 2.1 Customized HTTP response objects
    
1. Success http response objects
    - HttpOK -> 200[OK] (Default success status code)
    - HttpCreated -> 201[Created] (Resource creation success status code)
    - HttpUpdated -> 202[Accepted] (Resource update success status code)
    - HttpDeleted ->  203[No Content] (Resource delete success status code)

2. Error http response objects

    - HttpBadRequestError -> 400[Bad Request] (Invalid request payload/body validation status code)
    - HttpUnauthorizedError -> 401[Unauthorized] (Authentication/Authorization failed status code)
    - HttpInvalidDataError -> 422[Unprocessable Entity Code] (Request payload/body data validation failed status code)
    - HttpNotFoundError -> 404[Not Found] (Default route not exist/Resource detail endpoint object not found status code)
    - HttpMethodNotAllowedError -> 405[Method Not Allowed] (framework auto-management)


### 2.2 Http response object usage in HTTP-REQUEST-LIFECYCLE sugguestion

<pre>
request --> endpoint 
            |
            |-> 1. {if not authorized() ==> raise HttpUnauthorizedError(body:Union[Dict, str])} # for endpoint which need access permission
            |
            |-> 2. {if not request.json ==> raise HttpBadRequestError(body:Union[Dict, str])} # for the endpoint which expected some request data/body
            |
            |-> 3. {if not valid(payload or body) ===> raise HttpInvalidDataError(body:Union[Dict, str])} # for the endpoint which will validate the request data/body
            |
            |-> 4. {if not get_object_by_id(some_id) ===> raise HttpNotFoundError(body:Union[Dict, str])} # for the endpoint which get the detail of some resource by id but not found/any other exception
            |
            |-> 5. {if created_object(payload/body) ===> return HttpCreated(body:Union[Dict, str])} # for the endpoint which used to create resource object
            |
            |-> 6. {if updated_object(payload/body) ===> return HttpUpdated(body:Union[Dict, str])} # for the endpoint wich used to update resource object
            |
            |-> 7. {if deleted_object(payload/body) ===> return HttpUpdated(body:Union[Dict, str])} # for the endpoint which used to delete resource object 
            
</pre>


### 2.3 Http response object's BODY-PARAMETER specification

As we saw above, all our http response objects can accept both a *Dict* ou a *String* object as their body. 

To have a good communication among all of us, here is my specification sugguestions
about the behaviour on resonse's body value

1. For the *ERROR* http response objects, we can use the following example 

> 1. When we want to just use a string object as response body, use `raise HttpUnauthorizedError('current access is not permmited')` 

> 2. When we want to use a Dict object as response body, use `raise HttpUnauthorizedError({'message':'current access is not permmited'})` 

> 3. The 2 cases above will render a same response body `{'status':401, 'message':'current access is not permmited'}` to our client<web/mobile>

2. For the *SUCCESS* http response objects, we just let the framework to handle them as default



### 2.4 Http response object's FINAL-RENDER-DATA-STRUCTURE specification

To improve our communication experience, I have the fowlling sugguestions about the response object's data structure for our client __<web/mobile>__:

> 1. In general, our response object will contain at least `{'status':xxx, 'message':''}`

> 2. For our *ERROR* Http response object, the message must not be empty: `{'status':401, 'message':'current access is not permmited'}`

> 3. For our *NON-ERROR* http response object, the message can be empty or some pre-defined value: `{'status':201, 'message':''}`  or `{'status':201, 'message':'Created'}`

> 4. For our *RESOURCE-LIST(GET /v1/resource_names)* http response object, our response object body should extended to be some like: `{'status':200, message:'', 'pagination':{}, 'objects':[]}`

> 5. For our *RESOURCE-DETAIL(GET /v1/resource/resource_id)* http response object, our response object body should extended to be something like: `{'status':200, message:'', 'object':{}}`


### 2.5 Http request url pattern specification

To strength the concept of micro-service, I would like to point the url pattern specification recommendations

When our client access our endpoint in backend, normally we have the following access cases:
1. Access multiple object collections
2. Access single object collections
3. Do operation on multiple objects
4. Do operation on single object

In case to avoid lost when we navigate all our services, it will be good to separate the responsibility of each project by isolate the resource inside the project. To realize this, I sugguest use namespace on url define pattern, here is my sugguestions:

1. When we want to access multiple objects resource from a service, use plural name in url path:
    > `METHOD /v0/drivers`

2. When we want to access single object resource from a service,  use single name in url path:
    > `METHOD /v0/driver/<driver-id>`

3. When we want to do operation on multiple objects resource of a service, avoid use outside resource name to polute url path, use payload instead of url path parameter:
    > `METHOD /v0/drivers/<upload>`

4. When we want to do operation on a single object resource of a service, avoid use outside resource name to polute url path, use payload instead of url path:
    > `METHOD /v0/driver/<driver-id>/favorite`

5. *NOTE* avoid use one resource name in any other resource's access url path:
    > `METHOD /v0/driver/<driver-id>/<office-id>/operations`




### 2.6 Http request endpoint(callback)'s responsibility specifications

1. Resource list endpoint

> - Method: `GET` 
> - URL: `/v1/resource_names`
> - Responsibilities:
>   - Can validate authorization automatically
>   - Can filter target objects by authorization information automatically
>   - Can filter the target object by query parameters: `?field_name=value`
>   - Can filter the target object by some extra operation: `?field_name__gt=value` 
>   - Can sort the target object by specified field&order&combinations: `?sort=name&sort=-created_at` , `?sort=[name,-created_at]`
>   - Can exclude fields from the target object: `?exclude=age&exclude=password`, `?exclude=[age,password]`
>   - Can include fields from the target object: `?include=age&include=password`, `?include=[name, age]`
>   - Can specify paginations: `?page=0&limit=20`
>   - Can use full-text search: `?q=keyword`
> - Responses:
>   - status: 200 / body: {'status':200, 'message':'', 'pagination':{}, 'objects':[]}
>   - status: 401 / body: {'status':401, 'message':'Access not allowed'}

2. Resource detail endpoint 

> - Method: `GET` 
> - URL: `/v1/resource_name/<resource_id>`
> - Responsibilities:
>   - Can validate authorization automatically
>   - Can only filter one target by resource_id
>   - Can validate the authoriztion accessibility automatically 
>   - Can exclude fields from target object: `?exclude=age&exclude=password` or `?exclude=[age,password]`
>   - Can include fields from the target object: `?include=name&include=age`, `?include=[name, age]`
> - Responses:
>   - status: 200 / body: {'status':200, 'message':'',  'object':{}}
>   - status: 401 / body: {'status':401, 'message':'Access not allowed'}
>   - status: 404 / body: {'status':404, 'message':'Object not found'}


3. Resource Create endoint  

> - Method: `POST` 
> - URL: `/v1/resource_names`
> - Responsibilities
>   - Can validate authorization automatically
>   - Can create one/many objects 
>   - Can return the new created object by query parameter: `?return_new=true`
> - Responses:
>   - status: 201 / body: {'status':201, 'message':'',  'object':{'id':'xxxx',...}}
>   - status: 201 / body: {'status':201, 'message':'',  'objects':[{'id':'xxxx',...}]}
>   - status: 400 / body: {'status':400, 'message':'Bad request data'}
>   - status: 401 / body: {'status':401, 'message':'Access not allowed'}
>   - status: 422 / body: {'status':422, 'message':{'contact':{'name':['name is required']}}}

4. Resource update endpoint 

> - Method: `PUT/PATCH` 
> - URL: `/v1/resource_name/<resource_id>`
> - Responsibilities:
>   - Can validate authorization automatically
>   - Can update the target object totally or partially with payload
>   - Can return the new updated object by query_paramter: `?return_new=true`
> - Responses:
>   - status: 202 / body: {'status':202, 'message':'',  'object':{'id':'xxxx',...}}
>   - status: 400 / body: {'status':400, 'message':'Bad request data'}
>   - status: 401 / body: {'status':401, 'message':'Access not allowed'}
>   - status: 404 / body: {'status':404, 'message':'Object not found'}
>   - status: 422 / body: {'status':422, 'message':{'contact':{'name':['name is required']}}}

5. Resource delete endpoint

> - Method: `DELETE` 
> - URL: `/v1/resource_name/<resource_id>`
> - Responsibilities:
>   - Can validate authorization automatically
>   - Can remove the object from database by the given *resource_id*
> - Responses:
>   - status: 204 / body: {'status':204, 'message':'Deleted' }
>   - status: 401 / body: {'status':401, 'message':'Access not allowed'}
>   - status: 404 / body: {'status':404, 'message':'Object not found'}


### 2.7 Handle Server Error 500 

Since server error is very important but happens only coincidentally, I sugguest we can put a channel notification on 500 error handle process. So when it happens our devs can be warned imediately.



## 3. Http request object recommendations

### 3.1 Request Header specifications

In general, the http header are used to give us some information about our service access client, the most common case is authentication/authorization.
But sometimes, our system need some extra non-standard headers for reason like statistic, track, etc. 

So here i just make a simple note, more details need to be confirmed with everyone:

| header      | description                                       | value            |
|-------------|---------------------------------------------------|------------------|
|Authorization|An authentication token used to access our service |'Bearer xxxxxxxxx'| 
|source       |A string value to identify where is our client from|'web','app','xxxx'|

### 3.2 Request source types specifications

## 4. Http restful resource object recommendations

### 4.1 Resourc types
