#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 19/05/2020  20:52
@Author: liang
@File: test_detail_resource_api_demo.py
"""

from webtest import TestResponse

from project.locales import el
from tests.base_api_test import TestDemoApiBase

_ = el.gettext


class TestGetDetailApiDemo(TestDemoApiBase):

    def setUp(self) -> None:
        super(TestGetDetailApiDemo, self).setUp()

        for obj in self.MOCK_DEMO_OBJECTS[::2]:
            self.app.post_json('/v0/demos', obj, headers=self.valid_admin_header)

        for obj in self.MOCK_DEMO_OBJECTS[1::2]:
            self.app.post_json('/v0/demos', obj, headers=self.valid_operator_header)

        self.admin_created_resource_id = self.MOCK_DEMO_OBJECTS[0]['id']
        self.operator_created_resource_id = self.MOCK_DEMO_OBJECTS[1]['id']
        self.base_url = '/v0/demo/{}'

    def test_access_none_exist_object_failed(self):
        self.response = self.app.get(self.base_url.format(self.non_exist_resource_id),
                                     headers=self.valid_operator_header,
                                     status=404)
        self.assert_response_contains_common_keys(self.response)

    def test_operator_access_admin_created_object_failed(self):
        self.response = self.app.get(self.base_url.format(self.admin_created_resource_id),
                                     headers=self.valid_operator_header,
                                     status=403)
        self.assert_response_contains_common_keys(self.response)

    def test_operator_access_own_object_ok(self):
        self.response = self.app.get(self.base_url.format(self.operator_created_resource_id),
                                     headers=self.valid_operator_header, status=200)
        self.assert_response_contains_common_keys(self.response)

    def test_admin_access_operator_created_object_ok(self):
        self.response = self.app.get(self.base_url.format(self.operator_created_resource_id),
                                     headers=self.valid_admin_header, status=200)
        self.assert_response_contains_common_keys(self.response)

    def test_admin_access_own_object_ok(self):
        self.response = self.app.get(self.base_url.format(self.admin_created_resource_id),
                                     headers=self.valid_admin_header, status=200)

        self.assert_response_contains_common_keys(self.response)
