#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 10/06/2020  23:20
@Author: liang
@File: test_global_setting_module_factory.py
"""
import pytest

from project.feature_mock import ModuleSettings


def test_global_settings():
    ModuleSettings('tests.test_global_settings.settings')
    from . import settings
    assert settings.API_ENV == 'test'

    assert settings.ORIGINAL_BOOL is True
    assert settings.ORIGINAL_FLOAT == 15.5
    assert settings.ORIGINAL_INTEGER == 20
    assert settings.ORIGINAL_STRING == 'original string'
    assert settings.ORIGINAL_LIST == ['original', 'list']
    assert settings.ORIGINAL_SET == {'original', 'set'}
    assert settings.ORIGINAL_TUPLE == ('original', 'tuple')
    assert settings.ORIGINAL_DICT == {'original': 'dict'}

    assert settings.ENV_NO_DEFAULT is None
    assert settings.ENV_WITH_DEFAULT_NO_TYPE == 'env_default_str_no_type'
    assert settings.ENV_WITH_DEFAULT_STR_TYPE == 'env_default_str_str_type'
    assert settings.ENV_WITH_DEFAULT_INT_TYPE == 234
    assert settings.ENV_WITH_DEFAULT_BOOL_TYPE is False
    assert settings.ENV_WITH_DEFAULT_FLOAT_TYPE == 12.5

    assert settings.FILE_STR_INT == '123'
    assert settings.FILE_REAL_INT == 123
    assert settings.FILE_DEFAULT_STR == 'default_value'
    assert settings.FILE_STR_BOOL == 'false'
    assert settings.FILE_REAL_BOOL is False
    assert settings.FILE_STR_FLOAT == '12.5'
    assert settings.FILE_REAL_FLOAT == 12.5


def test_non_declared_module_settings_failed():
    with pytest.raises(Warning) as e:
        ModuleSettings('tests.test_global_settings.non_declared_module_settings', debug=True)
    assert 'Configuration fields' in str(e)


def test_invalid_section_setting_file_failed():
    with pytest.raises(Warning) as e:
        ModuleSettings('tests.test_global_settings.invalid_section_config_settings', debug=True)

    assert 'Invalid config sections' in str(e)


def test_non_exist_config_setting_file_failed():
    with pytest.raises(Warning) as e:
        ModuleSettings('tests.test_global_settings.non_exist_config_file_settings', debug=True)

    assert 'not found' in str(e)


def test_improve_coverage_ok():
    ModuleSettings('tests.test_global_settings.settings', api_env_key='ENV')

    ModuleSettings('tests.test_global_settings.settings', valid_envs=['default', 'dev', 'test'])

    ModuleSettings('tests.test_global_settings.settings', config_file_key='__config_files__')


def test_setting_module_not_found_failed():
    with pytest.raises(ModuleNotFoundError):
        ModuleSettings('tests.test_global_settings.non_exists_settings_module')
