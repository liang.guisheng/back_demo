#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 10/06/2020  23:20
@Author: liang
@File: settings.py
"""

API_ENV = '${API_ENV:test}'

# these options will keep the same value after patch the global setting factory
ORIGINAL_INTEGER = 20
ORIGINAL_FLOAT = 15.5
ORIGINAL_BOOL = True
ORIGINAL_STRING = 'original string'
ORIGINAL_LIST = ['original', 'list']
ORIGINAL_SET = {'original', 'set'}
ORIGINAL_TUPLE = ('original', 'tuple')
ORIGINAL_DICT = {'original': 'dict'}

# these options will pass the global setting factory with and convert to right value type
ENV_NO_DEFAULT = '${ENV_NO_DEFAULT}'
ENV_WITH_DEFAULT_NO_TYPE = '${ENV_WITH_DEFAULT_NO_TYPE:env_default_str_no_type}'
ENV_WITH_DEFAULT_STR_TYPE = '${ENV_WITH_DEFAULT_STR_TYPE:env_default_str_str_type<str>}'
ENV_WITH_DEFAULT_INT_TYPE = '${ENV_WITH_DEFAULT_INT_TYPE:234<int>}'
ENV_WITH_DEFAULT_BOOL_TYPE = '${ENV_WITH_DEFAULT_BOOL_TYPE:false<bool>}'
ENV_WITH_DEFAULT_FLOAT_TYPE = '${ENV_WITH_DEFAULT_FLOAT_TYPE:12.5<float>}'

_config_files_ = ['config/config.ini']
# these options are load from configuration file but should be declared here explicit
FILE_DEFAULT_STR = None
FILE_REAL_INT = None
FILE_STR_INT = None
FILE_STR_FLOAT = None
FILE_REAL_FLOAT = None
FILE_STR_BOOL = None
FILE_REAL_BOOL = None
