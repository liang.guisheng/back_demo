#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 19/05/2020  20:52
@Author: liang
@File: test_detail_resource_api_demo.py
"""

from project.locales import el
from tests.base_api_test import TestDemoApiBase

_ = el.gettext


class TestUpdateApiDemo(TestDemoApiBase):

    def setUp(self) -> None:
        super(TestUpdateApiDemo, self).setUp()
        for obj in self.MOCK_DEMO_OBJECTS[::2]:
            self.app.post_json('/v0/demos', obj, headers=self.valid_admin_header)

        for obj in self.MOCK_DEMO_OBJECTS[1::2]:
            self.app.post_json('/v0/demos', obj, headers=self.valid_operator_header)

        self.admin_created_resource_id = self.MOCK_DEMO_OBJECTS[0]['id']
        self.operator_created_resource_id = self.MOCK_DEMO_OBJECTS[1]['id']
        self.base_url = '/v0/demo/{}'

    def test_update_without_authentication_failed(self):
        self.response = self.app.put_json(self.base_url.format(self.non_exist_resource_id), None,
                                          status=403)
        self.assert_response_contains_common_keys(self.response)

    def test_update_with_bad_request_failed(self):
        self.response = self.app.put_json(self.base_url.format(self.non_exist_resource_id), None,
                                          headers=self.valid_operator_header, status=400)
        self.assert_response_contains_common_keys(self.response)

    def test_update_with_invalid_data_failed(self):
        self.response = self.app.put_json(self.base_url.format(self.non_exist_resource_id),
                                          {'id': 'any non uuid value'}, headers=self.valid_operator_header,
                                          status=422)
        self.assert_response_contains_common_keys(self.response)

    def test_update_with_non_exist_resource_failed(self):
        self.response = self.app.put_json(self.base_url.format(self.non_exist_resource_id),
                                          {'name': 'anyone'},
                                          headers=self.valid_operator_header,
                                          status=404
                                          )
        self.assert_response_contains_common_keys(self.response)

    def test_operator_update_admin_create_resource_failed(self):
        self.response = self.app.put_json(self.base_url.format(self.admin_created_resource_id),
                                          {'name': 'anyone'},
                                          headers=self.valid_operator_header,
                                          status=403
                                          )
        self.assert_response_contains_common_keys(self.response)

    def test_operator_update_own_resource_ok(self):
        self.response = self.app.put_json(self.base_url.format(self.operator_created_resource_id),
                                          {'name': 'anyone'},
                                          headers=self.valid_operator_header,
                                          status=202
                                          )
        self.assert_response_contains_common_keys(self.response)
        assert 'name' not in self.response.json.get('object')

        self.response = self.app.put_json(self.base_url.format(self.operator_created_resource_id) + '?return_new=true',
                                          {'name': 'anyone'},
                                          headers=self.valid_operator_header,
                                          status=202
                                          )
        self.assert_response_contains_common_keys(self.response)

        assert 'name' in self.response.json.get('object')
        assert self.response.json.get('object').get('update_log').get('id') == self.valid_operator_header.get(
            'x-user-id')

    def test_admin_update_operator_created_resource_ok(self):
        self.response = self.app.put_json(self.base_url.format(self.operator_created_resource_id),
                                          {'name': 'anyone'},
                                          headers=self.valid_admin_header,
                                          status=202
                                          )
        self.assert_response_contains_common_keys(self.response)
        assert 'name' not in self.response.json.get('object')

        self.response = self.app.put_json(self.base_url.format(self.operator_created_resource_id) + '?return_new=true',
                                          {'name': 'anyone2'},
                                          headers=self.valid_admin_header,
                                          status=202
                                          )
        self.assert_response_contains_common_keys(self.response)
        assert self.response.json.get('object').get('name') == 'anyone2'
        assert self.response.json.get('object').get('update_log').get('id') == self.valid_admin_header.get('x-user-id')

    def test_admin_update_own_resource_ok(self):
        self.response = self.app.put_json(self.base_url.format(self.admin_created_resource_id),
                                          {'name': 'anyone'},
                                          headers=self.valid_admin_header,
                                          status=202
                                          )
        self.assert_response_contains_common_keys(self.response)
        assert 'name' not in self.response.json.get('object')

        self.response = self.app.put_json(self.base_url.format(self.admin_created_resource_id) + '?return_new=true',
                                          {'name': 'anyone2'},
                                          headers=self.valid_admin_header,
                                          status=202
                                          )
        self.assert_response_contains_common_keys(self.response)
        assert self.response.json.get('object').get('name') == 'anyone2'
        assert self.response.json.get('object').get('update_log').get('id') == self.valid_admin_header.get('x-user-id')
