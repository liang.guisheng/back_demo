#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 19/05/2020  20:52
@Author: liang
@File: test_create_resource_api_demo.py
"""
from uuid import uuid4

from project.locales import el
from tests.base_api_test import TestDemoApiBase

_ = el.gettext


class TestCreateApiDemo(TestDemoApiBase):

    def setUp(self) -> None:
        super(TestCreateApiDemo, self).setUp()
        self.base_url = '/v0/demos'

    def test_create_resource_without_authentication_failed(self):
        self.response = self.app.post_json(self.base_url,
                                           None,
                                           status=403)
        self.assert_response_contains_common_keys(self.response)
        assert self.response.json.get('message') == _('unauthenticated_user')

    def test_create_resource_with_bad_request_failed(self):
        self.response = self.app.post_json(self.base_url,
                                           None,
                                           headers=self.valid_admin_header,
                                           status=400)
        self.assert_response_contains_common_keys(self.response)
        assert self.response.json.get('message') == _('bad_request_data')

    def test_create_resource_with_invalid_data_failed(self):
        self.response = self.app.post_json(self.base_url,
                                           {'id': 123},
                                           headers=self.valid_admin_header,
                                           status=422)
        self.assert_response_contains_common_keys(self.response)
        for key in ['id', 'name']:
            assert key in self.response.json.get('message')

    def test_create_resource_success_but_not_return_new_object_ok(self):
        self.response = self.app.post_json(self.base_url,
                                           {'id': str(uuid4()), 'name': 'test'},
                                           headers=self.valid_admin_header,
                                           status=201
                                           )

        self.assert_response_contains_common_keys(self.response)
        assert 'object' in self.response.json

        assert len(self.response.json.get('object')) == 1 and 'id' in self.response.json.get('object')

    def test_create_resource_success_with_return_new_object_ok(self):
        self.response = self.app.post_json(f'{self.base_url}?return_new=true',
                                           {'id': str(uuid4()), 'name': 'test'},
                                           headers=self.valid_admin_header,
                                           status=201
                                           )

        self.assert_response_contains_common_keys(self.response)

        # NOTE: creation endpoint success response should be always have 'object' field
        assert 'object' in self.response.json

        # NOTE: creation endpoint with 'return_new=true', will include other information instead of only id of object
        assert 'name' in self.response.json.get('object')
