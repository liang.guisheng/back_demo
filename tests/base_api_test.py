#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 23/05/2020  21:00
@Author: liang
@File: base_api_test.py
"""
import random
import unittest
from uuid import uuid4

from pyparsing import Optional
from webtest import TestApp, TestResponse

from project.app import App
from project.schema.demo_schema_v0 import DemoSchema


class TestDemoApiBase(unittest.TestCase):

    def setUp(self) -> None:
        self.COMMON_CREATE_RESPONSE_KEYS = ['status', 'message']
        self.MOCK_DEMO_OBJECTS = [
            {
                'id': str(uuid4()),
                'name': ''.join(random.sample('abcdefghiklmnopqrestuvwxyz', 5))
            }
            for i in range(10)
        ]
        self.app = TestApp(App)
        self.valid_admin_header = {'x-user-id': str(uuid4()), 'x-user-role': 'admin', 'x-user-type': 'admin'}
        self.valid_operator_header = {'x-user-id': str(uuid4()), 'x-user-role': 'operator', 'x-user-type': 'operator'}
        self.response: Optional[TestResponse] = None
        self.non_exist_resource_id = str(uuid4())

    def tearDown(self) -> None:
        DemoSchema.db().delete_many({})

    def test_non_http_response_or_error_failed(self):
        with self.assertRaises(Exception):
            response = self.app.get('/v0/demo/error_case')
            self.assert_response_contains_common_keys(response)

    def assert_response_contains_common_keys(self, response: TestResponse):
        """
        Make sure all kind of http response's result contains the common fields: status & message
        """
        for key in self.COMMON_CREATE_RESPONSE_KEYS:
            assert key in response.json
