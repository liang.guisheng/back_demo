#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 23/05/2020  21:03
@Author: liang
@File: test_delete_resource_api_demo.py
"""

from project.locales import el
from tests.base_api_test import TestDemoApiBase

_ = el.gettext


class TestDeleteApiDemo(TestDemoApiBase):

    def setUp(self) -> None:
        super(TestDeleteApiDemo, self).setUp()

        for obj in self.MOCK_DEMO_OBJECTS[0::2]:
            self.app.post_json('/v0/demos', obj, headers=self.valid_admin_header)

        for obj in self.MOCK_DEMO_OBJECTS[1::2]:
            self.app.post_json('/v0/demos', obj, headers=self.valid_operator_header)

        self.admin_created_resource_id = self.MOCK_DEMO_OBJECTS[0]['id']
        self.operator_created_resource_id = self.MOCK_DEMO_OBJECTS[1]['id']
        self.base_url = '/v0/demo/{}'

    def test_delete_resource_without_authentication_failed(self):
        self.response = self.app.delete(self.base_url.format(self.non_exist_resource_id),
                                        status=403)
        self.assert_response_contains_common_keys(self.response)

    def test_operator_delete_resource_non_exist_failed(self):
        self.response = self.app.delete(self.base_url.format(self.non_exist_resource_id),
                                        headers=self.valid_admin_header,
                                        status=404)
        self.assert_response_contains_common_keys(self.response)

    def test_admin_delete_resource_non_exist_failed(self):
        self.response = self.app.delete(self.base_url.format(self.non_exist_resource_id),
                                        headers=self.valid_admin_header,
                                        status=404)
        self.assert_response_contains_common_keys(self.response)

    def test_operator_delete_admin_created_resource_failed(self):
        self.response = self.app.delete(self.base_url.format(self.admin_created_resource_id),
                                        headers=self.valid_operator_header,
                                        status=403)
        self.assert_response_contains_common_keys(self.response)

    def test_operator_delete_own_resource_ok(self):
        self.response = self.app.delete(self.base_url.format(self.operator_created_resource_id),
                                        headers=self.valid_operator_header,
                                        status=203)
        self.assert_response_contains_common_keys(self.response)

    def test_admin_delete_operator_created_resource_ok(self):
        self.response = self.app.delete(self.base_url.format(self.operator_created_resource_id),
                                        headers=self.valid_admin_header,
                                        status=203)
        self.assert_response_contains_common_keys(self.response)

    def test_admin_delete_own_resource_ok(self):
        self.response = self.app.delete(self.base_url.format(self.admin_created_resource_id),
                                        headers=self.valid_admin_header,
                                        status=203)
        self.assert_response_contains_common_keys(self.response)
