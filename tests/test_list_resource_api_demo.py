#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 19/05/2020  20:52
@Author: liang
@File: test_list_resource_api_demo.py
"""

from project.locales import el
from tests.base_api_test import TestDemoApiBase

_ = el.gettext


class TestListApiDemo(TestDemoApiBase):

    def setUp(self) -> None:
        super(TestListApiDemo, self).setUp()
        self.base_url = '/v0/demos'
        for obj in self.MOCK_DEMO_OBJECTS[::2]:
            self.app.post_json(self.base_url, obj, headers=self.valid_admin_header)
        for obj in self.MOCK_DEMO_OBJECTS[1::2]:
            self.app.post_json(self.base_url, obj, headers=self.valid_operator_header)

    def test_access_list_resource_without_authentication_failed(self):
        self.response = self.app.get(self.base_url, status=403)
        self.assert_response_contains_common_keys(self.response)

    def test_operator_list_only_own_resource_ok(self):
        self.response = self.app.get(self.base_url, headers=self.valid_operator_header, status=200)
        self.assert_response_contains_common_keys(self.response)
        assert 'objects' in self.response.json
        assert 0 < len(self.response.json.get('objects')) <= len(self.MOCK_DEMO_OBJECTS) / 2

    def test_admin_list_all_resource_ok(self):
        self.response = self.app.get(self.base_url, headers=self.valid_admin_header, status=200)
        self.assert_response_contains_common_keys(self.response)
        assert 'objects' in self.response.json
        assert len(self.response.json.get('objects')) == len(self.MOCK_DEMO_OBJECTS)

    def test_list_with_include_filter_ok(self):
        self.response = self.app.get(self.base_url + '?include=id', headers=self.valid_operator_header,
                                     status=200)
        self.assert_response_contains_common_keys(self.response)

        for obj in self.response.json.get('objects'):
            assert len(obj) == 1 and 'id' in obj

    def test_list_with_exclude_filter_ok(self):
        self.response = self.app.get(self.base_url + '?exclude=name', headers=self.valid_operator_header,
                                     status=200)
        self.assert_response_contains_common_keys(self.response)
        for obj in self.response.json.get('objects'):
            assert 'name' not in obj
